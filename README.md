## Alictus Automation

An automation process that consist the following steps:

1.	Uploads a dataset to Google Drive via Google Drive API.

2.	Creates a folder that contains the datasets spreadsheet in it via API methods.

3.	Creates separate spreadsheet ﬁles for each campaign row in the dataset via Google Sheets API. For each campaign, it inserts a column called total budget ( total budget = total clicks * cpc ) which is calculated by an excel formula (=c2*e2).

4.	A slack bot called “campaign-bot” which monitors the whole process with the slack notiﬁcations.

5.	A socket-mode implementation of slack bot which handles slash commands.

----

## Slack Slash Commands

This command will return the total budget based on the chosen campaign name as a reply:
	
		/calculate_budget <campaign_name>

		(ex: /calculate_budget H)
		

This command plots a bar chart of total budgets of specified campaigns based on plotly package with different colors and sends this chart to channel as a PNG file.
	
		/bot_comparecampaigns  <campaign_names>

		(ex: /bot_comparecampaigns N C D)
	
----

## Install Requirements

Install the required packages that are used in the project by the following command (Python3):
	
	pip3 install -r requirements.txt
	
---

## Run the automation

Open two different terminal windows and execute the following commands at them seperately.

The following command creates a socket-mode listener to handle slack slash commands: 

	python3 socket-mode.py


The following command starts the automation process: 

	python3 alictus-automation.py

---

