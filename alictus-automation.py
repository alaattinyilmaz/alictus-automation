import slack
import os
from pathlib import Path
from dotenv import load_dotenv
from googleapiclient.discovery import build
from utils import *

env_path = Path(".") / '.env'
load_dotenv(dotenv_path=env_path)

# Initializing web client for slack
web_client = slack.WebClient(token=os.environ['SLACK_TOKEN'])

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']

# Getting credentials
creds = credentials(SCOPES)

# Initializing Google Drive service
drive_service = build('drive', 'v3', credentials=creds)

# Uploading dataset file into Google Drive
file_id = create_file(drive_service, web_client, "campaign-dataset.xlsx", "application/vnd.google-apps.spreadsheet")

# Creating datasets on Google Drive
folder_id = create_folder(drive_service, web_client, "datasets")

# Moving dataset file into datasets folder on Google Drive
move_folder(drive_service, file_id, folder_id)
web_client.chat_postMessage(channel='#alictus-automation', text="Campaign dataset file is moved to datasets folder.")

# Initializing Google Sheet service
sheet_service = build('sheets', 'v4', credentials=creds)
sheet = sheet_service.spreadsheets()

DATASET_SPREADSHEET_ID = file_id
DATASET_RANGE_NAME = 'Sheet1!A:F'

# Getting values from dataset
result = sheet.values().get(spreadsheetId=DATASET_SPREADSHEET_ID, range=DATASET_RANGE_NAME).execute()
values = result.get('values', [])

if not values:
    print('No data found.')
else:
    for i,row in enumerate(values):
        if(i == 0):
            continue
        spreadsheet = {'values': [values[0],row]}
        print(spreadsheet)

campaign_spreadsheet_ids = []
campaign_names = []

# Creating spreadsheet for each row of dataset
for v,val in enumerate(values):
    if(v==0):
        continue
        
    sheet_name = "campaign_"+val[0]
    
    row_spreadsheet = {
        'properties': {
            'title': sheet_name
        }
    }

    spreadsheet = sheet_service.spreadsheets().create(body=row_spreadsheet, fields='spreadsheetId').execute()

    web_client.chat_postMessage(channel='#alictus-automation', text="Spreadsheet for campaign "+val[0]+" is created.")

    campaign_names.append(val[0])
    campaign_spreadsheet_ids.append(spreadsheet.get('spreadsheetId'))


# Moving spreadsheets to datasets folder
for csi in campaign_spreadsheet_ids:
    move_folder(drive_service, csi, folder_id)

web_client.chat_postMessage(channel='#alictus-automation', text="Campaign Spreadsheets are moved to datasets folder.")
print("Campaign Spreadsheets are moved to datasets folder.")

# Writing rows to seperate campaign spreadsheets
for v,val in enumerate(values):
    if(v==0):
        continue
        
    spreadsheet_id = campaign_spreadsheet_ids[v-1]

    # The A1 notation of a range to search for a logical table of data.
    # Values will be appended after the last row of the table.
    range_ = 'A:G'

    # How the input data should be interpreted.
    value_input_option = 'USER_ENTERED' 

    # How the input data should be inserted.
    insert_data_option = 'OVERWRITE' 

    value_range_body = {
        "values": [
                   values[0]+['Total Budget'],
                   val+['=C2*E2']
                  ]
    }

    request = sheet_service.spreadsheets().values().append(spreadsheetId=spreadsheet_id, range=range_, valueInputOption=value_input_option, insertDataOption=insert_data_option, body=value_range_body)
    response = request.execute()
    
    web_client.chat_postMessage(channel='#alictus-automation', text="Campaign "+val[0]+" spreadsheet is filled.")
    print(response)

web_client.chat_postMessage(channel='#alictus-automation', text="Automation operations are completed!")

