from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

"""
Gets valid user authentications for Google Drive and Sheets based on API KEY
Parameters:
    SCOPES : list
        the permissions which will operate on Drive, list
Returns:
    creds : Google drive credientials object
        credentials
"""
def credentials(SCOPES):

    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    return creds

"""
Creates a file on Google Drive based on its mime_type and 
Returns:
    file_id : str
        Created file id in Google Drive
"""
def create_file(drive_service, web_client, file_name, mime_type):

    file_metadata = {
        'name': file_name,
        'mimeType': mime_type
    }

    media = MediaFileUpload(file_name,
                            mimetype=mime_type,
                            resumable=True)

    file = drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()

    file_id = file.get('id')


    print ('File ID: ', file_id)

    # Sending a chat message to channel 
    web_client.chat_postMessage(channel='#alictus-automation', text="File "+file_name+" is created.")

    return file_id

"""
Creates a folder on Google Drive with the specified name
Parameters:
    drive_service: Google Drive service object
        it will be used to operate on Google Drive
    web_client: Slack client object
        it will be used to send chat messages
    folder_name: str
        name of the folder which will be created

Returns:
    folder_id : str
        Created folder id on Google Drive
"""
def create_folder(drive_service, web_client, folder_name):
    
    file_metadata = {
        'name': folder_name,
        'mimeType': 'application/vnd.google-apps.folder'
    }

    file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()

    folder_id = file.get('id')
    print ('Folder ID: ', folder_id)
    
    # Sending a chat message to channel 
    web_client.chat_postMessage(channel='#alictus-automation', text="Folder "+folder_name+" is created.")


    return folder_id

"""
Moves a file in a folder on Google Drive
Parameters:
    drive_service: Google Drive service object
        it will be used to operate on Google Drive
    file_id : str
        file id to be moved
    folder_id : str
        the folder id where the file will be moved
"""
def move_folder(drive_service, file_id, folder_id):
    
    # Retrieve the existing parents to remove
    file = drive_service.files().get(fileId=file_id, fields='parents').execute()

    previous_parents = ",".join(file.get('parents'))

    # Move the file to the new folder
    file = drive_service.files().update(fileId=file_id, addParents=folder_id, 
                                        removeParents=previous_parents, fields='id, parents').execute()

"""
Searches a file name on Google Drive
Parameters:
    drive_service : Google Drive service object
        it will be used to operate on Google Drive
    query_name : str
        file name to be searched
    mime_type : str
        mime type of the file to be searched
Returns: str
    the first file id that is found by the query name 
    returns "NO_FILE_FOUND" if not found
"""
def search_file(drive_service, query_name, mime_type):

    page_token = None
    while True:
        response = drive_service.files().list(q="name='"+query_name+"' and mimeType='"+mime_type+"'",
                                              spaces='drive',
                                              fields='nextPageToken, files(id, name)',
                                              pageToken=page_token).execute()

        files = response.get('files', [])
            
        for file in files:
            # Process change
            print ('Found file: %s (%s)' % (file.get('name'), file.get('id')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    
    if(len(files)):
        return files[0].get('id')
    else:
        return "NO_FILE_FOUND"

"""
Searches a file name on Google Drive
Parameters:
    sheet : Google Sheet service object
        it will be used to operate on Google Sheets
    spreadsheet_id : str
        spreadsheet id name to be picked
    range_name : str
        cell ranges to be picked (ex: A2:F2)
Returns: list or str
    values 
        values in the found spreadsheet 
        returns "NO_VALUE_FOUND" if spreadsheet is not found
"""
def get_spreadsheet(sheet, spreadsheet_id, range_name):
    result = sheet.values().get(spreadsheetId=spreadsheet_id, range=range_name).execute()
    values = result.get('values', [])
    if(len(values)):
        return values
    else:
        return "NO_VALUE_FOUND"