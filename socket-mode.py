import os
import slack
from pathlib import Path
from dotenv import load_dotenv
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
import plotly.express as px
import pandas as pd
from slack.errors import SlackApiError

from googleapiclient.discovery import build
from utils import *

# SCOPES are the permissions that will be operated on Google Drive
# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
creds = credentials(SCOPES)
drive_service = build('drive', 'v3', credentials=creds)
sheet_service = build('sheets', 'v4', credentials=creds)

# Call the Sheets API
sheet = sheet_service.spreadsheets()

# SLACK_TOKEN and SLACK_APP_TOKEN are stored in .env file
env_path = Path(".") / '.env'
load_dotenv(dotenv_path=env_path)

# Install the Slack app and get xoxb- token in advance
app = App(token=os.environ["SLACK_TOKEN"])

bolt_client = app.client
web_client = slack.WebClient(token=os.environ['SLACK_TOKEN'])

channel_id = "#alictus-automation"

"""
A slack command handler : /calculate_budget
It calculates the budget of specified campaign
"""
@app.command("/calculate_budget")
def calculate_budget(ack, say, command):
    ack()
    # Searching for the specified campaign spreadsheet on Google Drive
    campaign_ss_id = search_file(drive_service, "campaign_"+command['text'], "application/vnd.google-apps.spreadsheet")

    if(campaign_ss_id != "NO_FILE_FOUND"):
        # Getting the G2 cell of found spreadsheet 
        budget_response = get_spreadsheet(sheet, campaign_ss_id,"G2")
        if(budget_response != "NO_VALUE_FOUND"):
            say("Total Budget for compaign "+command['text']+": "+budget_response[0][0])
        else:
            say("No budget value found.")
    else:
        say("Campaign could not found.")

"""
A slash command handler : /bot_comparecampaigns
It returns the specified campaign budgets and a 
bar chart with their total budgets to Slack channel
"""
@app.command("/bot_comparecampaigns")
def compare_campaigns(ack, say, command):
    ack()

    campaign_queries = command['text'].split()

    campaign_names = []
    campaign_budgets = []

    for cn in campaign_queries:

        # Searching for the specified campaign spreadsheet on Google Drive
        campaign_ss_id = search_file(drive_service, "campaign_"+cn, "application/vnd.google-apps.spreadsheet")
    
        if(campaign_ss_id != "NO_FILE_FOUND"):
            budget_response = get_spreadsheet(sheet, campaign_ss_id,"G2")

            if(budget_response != "NO_VALUE_FOUND"):
                campaign_names.append(cn)
                budget_float = float(budget_response[0][0].replace(',', '.'))
                campaign_budgets.append(budget_float)

                say("Total Budget for compaign "+cn+": "+budget_response[0][0])

            else:
                say("No budget value found for Campaign " + cn)

    if(len(campaign_names)):
        # Creating a DataFrame to visualize in plotly
        campaign_df = pd.DataFrame({"campaign_name":campaign_names,"budget":campaign_budgets})
        
        # Creating barchart with campaign names and their budgets in different colors
        fig = px.bar(campaign_df, x = "campaign_name", y="budget", color="campaign_name", title='Budget Comparisons of Campaigns')
        
        # Saving the chart as a PNG file
        fig.write_image("campaign-comp-fig.png")

        # The name of the file you're going to upload
        file_name = "./campaign-comp-fig.png"
        # ID of channel that you want to upload file to

        try:
            # Call the files.upload method using the WebClient
            # Uploading files requires the `files:write` scope
            result = web_client.files_upload(
                channels=channel_id,
                initial_comment="Campaign Graph Comparisons",
                file=file_name,
            )
            # Log the result
            print(result)

        except SlackApiError as e:
            print("Error uploading file: ",e)
    else:
        say("Campaigns could not found.")

    print(campaign_names)
    print(campaign_budgets)

if __name__ == "__main__":
    SocketModeHandler(app, os.environ["SLACK_APP_TOKEN"]).start()